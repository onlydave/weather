import React from "react";
import { ScrollView, View } from "react-native";

import WeatherContext from "../weather-proxy/context/weather.context";
import WeatherCard from "./weather-card";
import Forcast from "./forcast";

const Weather: React.SFC<{}> = () => (
  <WeatherContext.Consumer>
    {value => (
      <ScrollView style={{ paddingTop: 80 }}>
        {
          value.cities.map((city) => {
            if (!value.weather[city]) {
              return null;
            }
            return (
              <View style={{ paddingBottom: 60 }} key={city}>
                <WeatherCard key={city} name={city} {...value.weather[city]} />
                <Forcast forcast={value.forcast[city]} />
              </View>
            );
          })
        }
      </ScrollView>
    )}
  </WeatherContext.Consumer>
);

export default Weather;
