import React from "react";
import { Text, View, StyleSheet, Image } from "react-native";

import { IWeatherObject } from "../weather-proxy/context/weather.context";

interface IWeatherCard {
  name: string,
  weather: IWeatherObject[],
  temp: number,
}


const WeatherCard: React.SFC<IWeatherCard> = (props) => {
  const { weather, name, temp } = props;

  const weatherDescription = weather.map(conditions => conditions.description)

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Text style={styles.placeName}>
          {name.toUpperCase()}
          {` ${temp}`}
          °
        </Text>
        <View style={styles.iconContainer}>
          {weather.map(({ id, icon }) => (
            <Image key={id} style={styles.weatherIcon} source={{ uri: `https://openweathermap.org/img/w/${icon}.png` }} />
          ))}
        </View>
      </View>
      <Text>{weatherDescription.join("/")}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 20,
  },
  headerContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  iconContainer: {
    flexDirection: "row",
    justifyContent: "flex-end",
  },
  placeName: {
    fontSize: 32,
  },
  weatherIcon: {
    width: 50,
    height: 50,
  },
});

export default WeatherCard;
