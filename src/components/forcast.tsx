import React from "react";
import { Text, View, StyleSheet, Image, ScrollView } from "react-native";

import { ICityData } from "../weather-proxy/context/weather.context";

interface IWeatherCard {
  forcast?: ICityData[],
}


const WeatherCard: React.SFC<IWeatherCard> = (props) => {
  const { forcast = [] } = props;

  return (
    <ScrollView horizontal style={styles.container}>
      {forcast.map(day => (
        <View key={day.dateTime} style={styles.dayContainer}>
          <Image style={styles.weatherIcon} key={day.weather[0].icon} source={{ uri: `https://openweathermap.org/img/w/${day.weather[0].icon}.png` }} />
          <Text>{Math.round(day.temp)}°</Text>
        </View>
      ))}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 10,
    margin: 10,
  },
  dayContainer: {
    alignItems: "center",
  },
  iconContainer: {
    flexDirection: "row",
    justifyContent: "flex-end",
  },
  placeName: {
    fontSize: 32,
  },
  weatherIcon: {
    width: 50,
    height: 50,
  },
});

export default WeatherCard;
