import React from "react";
import { Button, TextInput, View, StyleSheet } from "react-native";

export default class App extends React.Component<{addCity: (city: string)=>void}, {text: string}> {
  state = {
    text: "",
  }

  onPress = () => {
    const { text } = this.state;
    const { addCity } = this.props;
    addCity(text || "New York");
  }

  render() {
    return (
      <View>
        <TextInput style={styles.textInput} onChangeText={text => this.setState({ text })} placeholder="City Name..." />
        <Button onPress={this.onPress} title="Add City" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  textInput: {
    padding: 10,
    borderWidth: StyleSheet.hairlineWidth,
  },
});
