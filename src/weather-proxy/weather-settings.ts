
export default {
  forcastEndpoint: "http://api.openweathermap.org/data/2.5/forecast?units=metric",
  weatherEndpoint: "http://api.openweathermap.org/data/2.5/weather?units=metric",
  weatherAppId: "bd8326266ffeb1b662cf75fadf5dee2a",
};
