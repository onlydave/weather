import React from "react";

export interface IWeatherObject {
  description: string,
  icon: string,
  id: number,
  main: string,
}

export interface ICityData {
  dateTime: number,
  humidity: number,
  temp: number,
  windSpeed: number,
  weather: IWeatherObject[]
}

export interface IWeatherContext {
  cities: string[],
  weather: {
    [city: string]: ICityData,
  },
  forcast: {
    [city: string]: ICityData[],
  },
}

const WeatherContext: React.Context<IWeatherContext> = React.createContext({
  cities: ["dublin", "paris", "denmark"],
  weather: {},
  forcast: {},
});

export default WeatherContext;
