import axios from "axios";
import weatherSettings from "../weather-settings";

const getWeatherUrl = (city: string) => `${weatherSettings.weatherEndpoint}&q=${city}&APPID=${weatherSettings.weatherAppId}`

export const getWeather = (city: string) => axios.get(getWeatherUrl(city));


export default {
  getWeather,
};
