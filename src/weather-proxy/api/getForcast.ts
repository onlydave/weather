import axios from "axios";
import weatherSettings from "../weather-settings";

const getForcastUrl = (city: string) => `${weatherSettings.forcastEndpoint}&q=${city}&APPID=${weatherSettings.weatherAppId}`;

export const getForcast = (city: string) => axios.get(getForcastUrl(city));


export default {
  getForcast,
};
