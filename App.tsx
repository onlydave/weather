import React from "react";

import Weather from "./src/components/weather";
import { getWeather } from "./src/weather-proxy/api/getWeather";
import { getForcast } from "./src/weather-proxy/api/getForcast";
import WeatherContext, { IWeatherContext } from "./src/weather-proxy/context/weather.context";
import AddPlace from "./src/components/add-place";

export default class App extends React.Component<{}, IWeatherContext> {
  state = {
    cities: ["dublin", "paris", "Copenhagen"],
    weather: {},
    forcast: {},
  }

  componentDidMount() {
    this.getData();
  }

  getData = () => {
    const { cities } = this.state;
    cities.forEach((city) => {
      getWeather(city).then(({ data }) => {
        const { weather } = this.state;
        this.setState({
          weather: {
            ...weather,
            [city]: {
              dateTime: data.dt,
              humidity: data.main.humidity,
              temp: data.main.temp,
              windSpeed: data.wind.speed,
              weather: data.weather,
            },
          },
        });
      });
      getForcast(city).then(({ data }) => {
        if (!data.list) return;
        const { forcast } = this.state;
        this.setState({
          forcast: {
            ...forcast,
            [city]: data.list.map((day: any) => ({
              dateTime: day.dt,
              humidity: day.main.humidity,
              temp: day.main.temp,
              windSpeed: day.wind.speed,
              weather: day.weather,
            })),
          },
        });
      });
    });
  }

  addCity = (city: string) => {
    const { cities } = this.state;
    this.setState({
      cities: [...cities, city],
    }, this.getData);
  }

  render() {
    return (
      <WeatherContext.Provider value={this.state}>
        <Weather />
        <AddPlace addCity={this.addCity} />
      </WeatherContext.Provider>
    );
  }
}
